### Block Cloudflare MITM Attack!!

`Let's fight against the growing MITM attack on the Internet.`

![](../../image/goodorbad.jpg)


```

This add-on will block, notify, or redirect your request if the target website is using Cloudflare.

The purpose of this browser add-on is to block Cloudflare sites.

The TLS protocol promises end-to-end encryption between the client and an authenticated, identified endpoint server. The browser’s lock icon is a UI widget which makes this promise to the user. Cloudflare is a mass-decryption chokepoint, which intercepts and decrypts the Web requests made by billions of people to millions of websites.
 
This add-on never send any data.
Your cloudflare-domain collection is yours.

```


- Looking for `Palemoon`? [Block Cloudflare Requests (Palemoon)](../../tool/block_cloudflare_requests_pm)
- Download add-on
  - From Git: [FirefoxESR](https://git.disroot.org/dCF/deCloudflare/raw/branch/master/addons/releases/bcma.xpi) / [Chromium / Edge](https://git.disroot.org/dCF/deCloudflare/raw/branch/master/addons/releases/bcma.crx)
