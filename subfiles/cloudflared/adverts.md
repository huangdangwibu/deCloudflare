## How many % of adverts and trackers are using Cloudflare?


- [Easylist](https://web.archive.org/web/20210516110248/https://easylist.to/)
```
EasyList is the primary filter list that removes most adverts from international webpages, including unwanted frames, images and objects.

EasyPrivacy is an optional supplementary filter list that completely removes all forms of tracking from the internet, including web bugs, tracking scripts and information collectors, thereby protecting your personal data.
```


We picked domain-blocking lines from the list and filtered out domains which has exception rules.
Here's the result.


| Adblock list | Domains Count | Cloudflare | % |
| --- | --- | --- | --- |
| [EasyList](https://easylist.to/easylist/easylist.txt) | 29,758 | 8,184 | 27.5% |
| [EasyPrivacy](https://easylist.to/easylist/easyprivacy.txt) | 17,489 | 5,367 | 30.7% |
| [AdGuard](https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt) | 45,944 | 8,715 | 19% |
| [AdAway](https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt) | 2,088 | 723 | 34.6% |
| Total | 65,057 | 16,558 | 25.5% |


### 25.5% of adverts and trackers are using Cloudflare.