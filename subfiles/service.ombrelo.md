# Ombrelo


![](../image/ss-sercxi.png)


**Ombrelo** (pronounce /omˈbrelo/) is the **world's first anti-cloudflare search engine** website primarily for [Tor](https://torproject.org) [users](https://ombrelo.pkduaxfk6lrmo2be4yr6ke5pqzsujihywsggfb7tocnz7x3vqqkhfoqd.onion/) and [clearnet user](https://en.wikipedia.org/wiki/Clearnet_(networking))[s](https://ombrelo.eu.org/), operating since the [year 2016](https://addons.thunderbird.net/en-us/firefox/addon/searxes/).
An anniversary day of the project is `February 2nd`.

It is fed by quality sources and rank down [Cloudflare sites](../cloudflare_users/domains) to bottom by default, thus you [avoid the risk](../README.md) and [inconvenience](../PEOPLE.md) of having [MITM traps](../README.md) littered throughout search results.
You can also open a cached version of the page by clicking the icon of the search result.
There are many options to choose from for example disabling Cloudflare ranking and rank down known [Tor-hostile](../anti-tor_users/domains) sites.
They also providing public API service for developers and also Lynx-site for [text-base browser](../readme/en.ethics.md#browser-vendor-discrimination) users.

The current logo resembles an umbrella. Developers said that they want to provide an _umbrella_ to protect people safe against heavy _rain_ caused by the _cloud_.


![](../image/ssprotect.jpg)



### About _.onion_ certificate warning

Tor Browser user may see this warning on first visit.

```
Warning: Potential Security Risk Ahead

Tor Browser detected a potential security threat and did not continue.
Someone could be trying to impersonate the site and you should not continue.
```

You can ignore this warning because onion service is [already encrypted](https://community.torproject.org/onion-services/overview/).
There are multiple reasons to use HTTPS over Tor.
At the moment the only way to get a _trusted_ SSL certificate for a .onion domain is to buy an EV (extended validation) certificate.
